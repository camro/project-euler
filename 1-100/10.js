// The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.

// Find the sum of all the primes below two million.

const UPPER_NUMBER = 2000000;

function isPrime( n ){
	if (n == 2)
		return true;

	// If even, return false
	if (n < 2 || n % 2 == 0 )
		return false;

	let sqrt = Math.sqrt( n );
	for ( var i = 3; i <= sqrt; i += 2){
		if( n % i == 0 ){
			return false;
		}
	}

	return true;
}

let sum = 0;

for (var i = 1; i < UPPER_NUMBER; i++){
	if (isPrime(i))
		sum += i;
}

console.info("The sum of all prime numbers below " + 2000000 + " is " + sum);