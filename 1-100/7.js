// By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that the 6th prime is 13.

// What is the 10 001st prime number?

const PRIME_TO_GET = 10001;

function isPrime( n ){
	if (n == 2)
		return true;

	// If even, return false
	if (n < 2 || n % 2 == 0 )
		return false;

	let sqrt = Math.sqrt( n );
	for ( var i = 3; i <= sqrt; i += 2){
		if( n % i == 0 ){
			return false;
		}
	}

	return true;
}

let primesFound = 0;

let currentNumber = 0;

while (primesFound < PRIME_TO_GET) {
	if (isPrime(currentNumber))
		primesFound++;
	
	currentNumber++;
};

// subtract 1
currentNumber--;

let suffix;

switch (PRIME_TO_GET.toString().slice(-1)){
	case '1':
		suffix = 'st';
		break;

	case '2':
		suffix = 'nd';
		break;
	
	case '3':
		suffix = 'rd';
		break;

	default:
		suffix = 'th';
}

console.info("The " + PRIME_TO_GET + suffix + " prime number is " + currentNumber);